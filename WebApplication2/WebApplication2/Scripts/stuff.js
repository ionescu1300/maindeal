/// <reference path="../node_modules/@types/react/index.d.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var counter = function (state, action) {
    if (state === void 0) { state = 0; }
    switch (action.type) {
        case 'INCREMENT_TODO':
            return state + 1;
        case 'DECREMENT_TODO':
            return state - 1;
        default:
            return state;
    }
};
var incrementTodo = {
    type: 'INCREMENT_ASYNC'
};
var decrementTodo = {
    type: 'DECREMENT_TODO'
};
function welcomeState() {
    return { count: store.getState() };
}
var Welcome = /** @class */ (function (_super) {
    __extends(Welcome, _super);
    function Welcome() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Welcome.prototype.render = function () {
        return React.createElement("h1", null,
            "Hello, ",
            welcomeState().count,
            " ");
    };
    return Welcome;
}(React.Component));
//===============================
var sagaMiddleware = ReduxSaga.default();
var store = Redux.createStore(counter, Redux.applyMiddleware(sagaMiddleware));
sagaMiddleware.run(watchIncrementAsync);
var render = function () {
    ReactDOM.render((React.createElement(Welcome, null)), document.getElementById("root"));
};
store.subscribe(function (val) { render(); });
render();
setTimeout(function () {
    store.dispatch(incrementTodo);
}, 200);
//# sourceMappingURL=stuff.js.map
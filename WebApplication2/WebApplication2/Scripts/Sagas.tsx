﻿// Our worker Saga: will perform the async increment task
 function* incrementAsync() {
    yield ReduxSaga.delay(7000)
     yield ReduxSaga.effects.put({ type: 'INCREMENT_TODO' })
}

// Our watcher Saga: spawn a new incrementAsync task on each INCREMENT_ASYNC
 function* watchIncrementAsync() {
    yield ReduxSaga.effects.takeEvery('INCREMENT_ASYNC', incrementAsync)
}
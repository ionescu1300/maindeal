/// <reference path="../node_modules/@types/react/index.d.ts" />


const counter = (state = 0, action) => {
    switch (action.type) {
        case 'INCREMENT_TODO':
            return state + 1;
        case 'DECREMENT_TODO':
            return state - 1;
        default:
            return state;
    }
};



declare var Redux: any;
declare var ReactDOM: any;
declare var ReduxSaga: any;

const incrementTodo = {
    type: 'INCREMENT_ASYNC'
};

const decrementTodo = {
    type: 'DECREMENT_TODO'
};
type WelcomeState = {
    count: number
}

function welcomeState(): WelcomeState {
    return { count: store.getState() } as WelcomeState;
}

class Welcome extends React.Component<any, any>  {
    render() {
        return <h1>Hello, {welcomeState().count} </h1>;
    }
}


//===============================


const sagaMiddleware = ReduxSaga.default()
const store = Redux.createStore(
    counter,
    Redux.applyMiddleware(sagaMiddleware)
)
sagaMiddleware.run(watchIncrementAsync);







const render = () => {
    ReactDOM.render(
        (<Welcome />),
        document.getElementById("root")
    );
}

store.subscribe(function (val) { render(); });
render();


setTimeout(function () {
    store.dispatch(incrementTodo)

},200)


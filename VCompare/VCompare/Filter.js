var shippingFilterCost = {
  "Name": "CalculateShippingCostsFilter",
  "InjectedServices": "product, shoppingBasket, CalculateProductShipping",
  "MainMethod": function (product, shoppingBasket,CalculateProductShipping) {
    var shippingCost = CalculateProductShipping(product.weight, shoppingBasket.locationToShipTo);
    product.ShippingCost = shippingCost;
    return product;
  }
}


var checkIfBuyerHasAVouche = {
  "Name": "CheckVoucher",
  "InjectedServices": "product, shoppingBasket, CheckVoucherValid,CalculateVoucher",
  "EntryCondition": function (product, shoppingBasket, CheckVoucherValid, CalculateVoucher) {
    var isVoucherValid = CheckVoucherValid(shoppingBasket.voucherCode);   
    return isVoucherValid;
    // we only go to the 'MainMethod' if entryCondition returns true;
  },
  "MainMethod": function (product, shoppingBasket, CheckVoucherValid, CalculateVoucher) {
    var voucherPrice = CalculateVoucher(shoppingBasket.voucherCode);
    product.voucherPriceReduction = voucherPrice;
    return product;
  }
}

//how do we do recommendations

// just rerun the entire pipeline with a different price or added benefit.
// have a recommendation map:

//increments 1.
// Price goes up by 200 euros and age > 50 and averagedetanlappoinmentyearly <= 3 
// recommend if ( get dental included.)

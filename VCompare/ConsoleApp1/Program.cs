using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        { 


            WindsorContainer container = new WindsorContainer();


            MethodInfo methodInfo = typeof(ComputePropertyBehavior).GetMethods().FirstOrDefault(x => x.Name == "MainMethod");

            ComputePropertyBehavior.MainMethod(null);

            Console.Write(GetParamName(methodInfo, 0));
            Console.ReadKey();
        }

        public static string GetParamName(System.Reflection.MethodInfo method, int index)
        {
            ParameterInfo retVal = null;

            if (method != null && method.GetParameters().Length > index)
                retVal = method.GetParameters()[index];

            //retVal.Member is object
            return retVal.Name;
        }
    }


    public class InsuranceProduct
    {
        public int Id { get; set; }
        
        public int PricePerYear { get; set; }
        
        public int NobodyFaultCoverage { get; set; }
        public int YourOwnFaultCoverage { get; set; }
        public int TheOpponentFaultCoverage { get; set; }



    }

    public class ComputePropertyBehavior
    {
        public static object MainMethod([InjectableProp(nameof(StartCompare), nameof(StartCompare.HomePostCode))]ReadOnlyProp<string> precomparemodel_comparevalue1)
        {
            return null;
        }
    }


    public class InjectableProp : Attribute
    {
        public InjectableProp(string contextModel, string contextModelProp)
        {
            
        }
    }
    
    public class ReadOnlyProp<T> {  }    
    public class BaseContextModel {  }
    public class StartCompare : BaseContextModel
    {
        public string LicensePlate { get; set; }
        public string HomePostCode { get; set; }
        public string CityWorkLocation { get; set; }
        public bool WorkCommuteCar { get; set; }
        public int KilometersYear { get; set; }       
    }
}
